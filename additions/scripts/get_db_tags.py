import re

with open('additions/scripts/latex2docbook.py') as file: body = file.read()
pattern = re.compile(r'<[^/].+?>')

tags = set()
for tag in re.findall(pattern, body):
    tags.add(tag)

tags.remove('<myq>')
tags.remove('<?xml version="1.0" encoding="UTF-8"?>')

tags = sorted(list(tags))

print(tags)
