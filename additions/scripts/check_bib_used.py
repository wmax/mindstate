import re

with open('book.tex') as file: body = file.read()
with open('bibliography.bib') as file: bib = file.read()

pattern = re.compile(r'@book{([^,]+?),')

def test_bibliosource_used():
    for bib_id in re.findall(pattern, bib):
        assert re.search(bib_id, body)
