import re
import bibtexparser

def remove_latex_escaping(data):
    data = re.sub(r'\\%', r'%', data)
    data = re.sub(r'\\_', r'_', data)
    return data

def replace_special_characters(data):
    data = re.sub(r'~', r'&#xA0;', data)
    return data

def replace_headers(data):
    data = re.sub(r'\\part{(.+?)}\s*\\label{(.+?)}',
                  r'\n<chapter xml:id="\2"><info><title>\1</title></info>\n', data, flags=re.DOTALL)
    data = re.sub(r'\\part\[.+?\]{(.+?)}\s*\\label{(.+?)}',
                  r'\n<chapter xml:id="\2"><info><title>\1</title></info>\n', data, flags=re.DOTALL)
    data = re.sub(r'\\chapter{(.+?)}\s*\\label{(.+?)}',
                  r'\n<sect1 xml:id="\2"><info><title>\1</title></info>\n', data, flags=re.DOTALL)
    data = re.sub(r'\\chapter\[.+?\]{(.+?)}\s*\\label{(.+?)}',
                  r'\n<sect1 xml:id="\2"><info><title>\1</title></info>\n', data, flags=re.DOTALL)
    data = re.sub(r'\\section{(.+?)}\s*\\label{(.+?)}',
                  r'\n<sect2 xml:id="\2"><info><title>\1</title></info>\n', data, flags=re.DOTALL)
    data = re.sub(r'\\subsection{(.+?)}\s*\\label{(.+?)}',
                  r'\n<sect3 xml:id="\2"><info><title>\1</title></info>\n', data, flags=re.DOTALL)
    data = re.sub(r'\\subsubsection{(.+?)}\s*\\label{(.+?)}',
                  r'\n<sect4 xml:id="\2"><info><title>\1</title></info>\n', data, flags=re.DOTALL)
    data = re.sub(r'\\myendpart{.*?}', r'</chapter>', data)
    data = re.sub(r'\\myendchapter{.*?}', r'</sect1>', data)
    data = re.sub(r'\\myendsection{.*?}', r'</sect2>', data)
    data = re.sub(r'\\myendsubsection{.*?}', r'</sect3>', data)
    data = re.sub(r'\\myendsubsubsection{.*?}', r'</sect4>', data)
    return data

def replace_para(data):
    data = re.sub(r'\\spar', r'<para>\n', data, flags=re.DOTALL)
    data = re.sub(r'\\epar', r'</para>\n', data, flags=re.DOTALL)
    return data

def replace_notes(data):
    data = re.sub(r'\\note{([^{]+?)}{([^{]+?)}', r'<note><title>\1</title><para>\2</para></note>\n', data, flags=re.DOTALL)
    return data

def replace_quotes(data):
    data = re.sub(r'(epigraph{[^}]+?}{[^}]*?)«(.+?)»([^}]*?})', r'\1<myq>\2</myq>\3', data, flags=re.DOTALL)
    data = re.sub(r'«(.+?)»', r'<quote>\1</quote>', data, flags=re.DOTALL)
    data = re.sub(r'<myq>(.+?)</myq>', r'«\1»', data, flags=re.DOTALL)
    data = re.sub(r'\\begin{quotation}(.+?)\\end{quotation}',
                  r'<blockquote><para>\1</para></blockquote>\n', data, flags=re.DOTALL)
    data = re.sub(r'\\epigraph{([^{]+?)}{([^{]+?)}', r'<epigraph><attribution>\2</attribution><para>\1</para></epigraph>\n', data, flags=re.DOTALL)
    data = re.sub(r'\\epigraph{(.+?)}', r'<epigraph><para>\1</para></epigraph>\n', data, flags=re.DOTALL)
    return data

def replace_cites(data):
    data = re.sub(r'\\citetitle{(.+?)}', r'<citation>\1</citation>', data, flags=re.DOTALL)
    data = re.sub(r'\\cite{(.+?)}', r'<citation>\1</citation>', data, flags=re.DOTALL)
    return data

def replace_footnote(data):
    data = re.sub(r'\\footnote{(.+?)}', r'<footnote><para>\1</para></footnote>', data, flags=re.DOTALL)
    return data

def replace_index_tags(data):
    data = re.sub(r'\\index{(.+?)}', r'<indexterm><primary>\1</primary></indexterm>', data, flags=re.DOTALL)
    return data

def replace_glossary_terms(data):
    data = re.sub(r'\\gl{([^}]+?)}{([^}]+?)}', r'<glossterm linkend="\1">\2</glossterm>', data, flags=re.DOTALL)
    return data

def replace_emphasis(data):
    data = re.sub(r'\\emphasis{(.+?)}', r'<emphasis role="strong">\1</emphasis>', data, flags=re.DOTALL)
    return data

def replace_ref(data):
    data = re.sub(r'\\fullref{(.+?)}', r'(см. <xref linkend="\1"/>)', data, flags=re.DOTALL)
    data = re.sub(r'\\simplefullref{(.+?)}', r'<xref linkend="\1"/>', data, flags=re.DOTALL)
    return data

def replace_url(data):
    data = re.sub(r'\\href{(.+?)}{(.+?)}', r'<link xlink:href="\1">\2</link>', data, flags=re.DOTALL)
    data = re.sub(r'\\url{(.+?)}', r'<link xlink:href="\1"/>', data, flags=re.DOTALL)
    return data

def replace_list(data):
    data = re.sub(r'\\item (.+?)(?=\\item|\\end)', r'<listitem><para>\1</para></listitem>\n', data, flags=re.DOTALL)

    data = re.sub(r'\\begin{itemize}', r'<itemizedlist>\n', data, flags=re.DOTALL)
    data = re.sub(r'\\end{itemize}', r'</itemizedlist>\n', data, flags=re.DOTALL)

    data = re.sub(r'\\begin{enumerate}', r'<orderedlist>\n', data, flags=re.DOTALL)
    data = re.sub(r'\\end{enumerate}', r'</orderedlist>\n', data, flags=re.DOTALL)

    return data

def replace_glossary_entry(data):
    data = re.sub(r'\\newglossaryentry{(.+?)}{\s*name=(.+?),\s*description={(.+?)}\s*}',
                  r'<glossentry xml:id="\1">\n<glossterm>\2</glossterm><glossdef><para>\3</para></glossdef></glossentry>\n', data, flags=re.DOTALL)
    return data

with open('meta/abstract.tex') as file: abstract = file.read()
with open('meta/author_email.tex') as file: author_email = file.read()
with open('meta/author_name.tex') as file: author_name = file.read()
with open('meta/author_sname.tex') as file: author_sname = file.read()
with open('meta/license.tex') as file: license = file.read()
with open('meta/name.tex') as file: name = file.read()
with open('meta/subtitle.tex') as file: subtitle = file.read()
with open('meta/site.tex') as file: site = file.read()

head = '''<?xml version="1.0" encoding="UTF-8"?>
<book xmlns="http://docbook.org/ns/docbook"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xml:id="mind_state" version="5.0" xml:lang="ru">
 <info>
  <title>%s</title>
  <subtitle>%s</subtitle>
  <abstract>
   <para>
%s
   </para>
  </abstract>
  <author>
   <personname>
     <firstname>%s</firstname>
     <surname>%s</surname>
   </personname>
   <uri type="website">%s</uri>
   <email>%s</email>
  </author>
  <copyright>
   <year>
    <?dbtimestamp format="Y"?>
   </year>
   <holder>%s %s</holder>
  </copyright>
  <legalnotice>
   <para>
%s
   </para>
  </legalnotice>
 </info>
''' % (name, subtitle, abstract, author_name, author_sname, site, author_email, author_name, author_sname, license)


with open('book.tex') as file: body = file.read()
# clean
body = re.sub(r'\\title.+?part\*', r'\\part', body, flags=re.DOTALL)
body = re.sub(r'\\markright{ПРЕДИСЛОВИЕ}', r'', body)
body = re.sub(r'\\addcontentsline{toc\}{part}{\\nameref{intro}}', r'', body)
body = re.sub(r'\\setcounter{enumi}{-1}', r'', body)

body = remove_latex_escaping(body)
body = replace_special_characters(body)
body = replace_headers(body)
body = replace_para(body)
body = replace_notes(body)
body = replace_glossary_terms(body)
body = replace_cites(body)
body = replace_quotes(body)
body = replace_footnote(body)
body = replace_index_tags(body)
body = replace_emphasis(body)
body = replace_ref(body)
body = replace_url(body)
body = replace_list(body)


with open('glossary.tex') as file: gloss_data = file.read()
with open('meta/gloss_name.tex') as file: gloss_name = file.read()
glossary = '''
  <index/>
  <glossary xml:id="main">
    <title>%s</title>
''' % gloss_name

gloss_data = remove_latex_escaping(gloss_data)
gloss_data = replace_cites(gloss_data)
gloss_data = replace_emphasis(gloss_data)
gloss_data = replace_glossary_terms(gloss_data)
gloss_data = replace_ref(gloss_data)
gloss_data = replace_url(gloss_data)
gloss_data = replace_glossary_entry(gloss_data)

glossary = glossary + gloss_data + '''
  </glossary>
'''

# If changes will needed use bibtexparser
with open('meta/bib_name.tex') as file: bib_name = file.read()
bibliography = '''
  <bibliography>
    <title>%s</title>
''' % bib_name

with open('auto.bib') as bibtex_file:
    bibtex_database = bibtexparser.load(bibtex_file)

for entry in bibtex_database.entries:
    bibliography += '<biblioentry>\n'
    bibliography += '<abbrev>%s</abbrev>\n' % entry['ID']
    bibliography += '<author><personname>%s</personname></author>\n' % entry['author']
    bibliography += '<title>%s</title>\n' % entry['title']
    if 'publisher' in entry:
        bibliography += '<publisher><publishername>%s</publishername></publisher>\n' % entry['publisher']
    if 'year' in entry:
        bibliography += '<pubdate>%s</pubdate>\n' % entry['year']
    if 'url' in entry:
        bibliography += '<bibliosource class="uri"><link xlink:href="%s"/></bibliosource>\n' % entry['url']
    bibliography += '</biblioentry>\n'

bibliography = bibliography + '''
  </bibliography>
'''

foot = '''
</book>
'''

print(''.join((head, body, glossary, bibliography, foot)))
