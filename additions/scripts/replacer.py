import re

file_name = 'main.tex'

with open(file_name) as file: file_data = file.read()

regex = re.compile(r'\b(патрид\w*?)\b', re.IGNORECASE)
str = regex.sub(r'\\glslink{patrid}{\1}', file_data)

with open(file_name, 'w') as file: file.write(str)

