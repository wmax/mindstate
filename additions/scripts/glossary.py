import re
import pymorphy2

morph = pymorphy2.MorphAnalyzer()

with open('glossary.tex') as file: gloss = file.read()
pattern = re.compile(r'\\newglossaryentry{(.+?)}{\s*name=(.+?),\s*description')

with open('book.tex') as file: book = file.read()

for (label, name) in re.findall(pattern, gloss):
    name = name.lower()
    if re.search(r'\s', name):
        continue
    word = morph.parse(name)[0]
    for parse in word.lexeme:
        book = re.sub(r'([^{])\b(%s)\b([^}])' % parse.word, r'\1\\gl{%s}{\2}\3' % label, book, flags=re.IGNORECASE)

print(book)
