import re

with open('book.tex') as file: body = file.read()
pattern = re.compile(r'(?:part|chapter|section|subsection|subsubsection){(.+?)}\s*\\label{(.+?)}')

for (name, label) in re.findall(pattern, body):
    print('    %s [label="%s"]' % (label, name))
