import re
import os
from datetime import datetime
from urllib.parse import urlparse
import bibtexparser


base_url = 'http://mindstate.info/links/'
wget_cmd = 'cd ../ext;wget -U mozilla --random-wait -q --domains=%s %s -p -k -e robots=off %s'
link_html = '''<!doctype html><html lang=ru><head><meta charset="utf-8"></head>
 <body>
  <p>Нажмите для перехода по <a href="%s">ссылке</a></p>
  <p>Если ссылка не открывается, то можете использовать <a href="%s">архивную копию</a> от %s</p>
 </body>
</html>
'''
dir = 'site/links/'
os.makedirs(dir, exist_ok=True)
now = datetime.today()

with open('bibliography.bib') as bibtex_file:
    bibtex_database = bibtexparser.load(bibtex_file)

for bib in bibtex_database.entries:
    if 'url' in bib:
        if not re.search(r'mindstate.info', bib['url']):
            url = bib['url']
            name = bib['ID'].lower()
            url_obj = urlparse(url)
            domain = url_obj.netloc
            # hack for multipage book
            recursive = '-r' if re.search(r'petrov.com.ua', url) else ''
            print('Download js page manually: %s' % url) if re.search(r'ted\.com', url) else None
            print('Download js page manually: %s' % url) if re.search(r't\.me', url) else None
            # download url
            if not os.path.exists('../ext/%s%s' % (domain, url_obj.path)):
                command = wget_cmd % (domain, recursive, url)
                os.system(command)
            # make html file with real and cached link
            link_file = dir + name
            cache_url = '/ext/' + domain + url_obj.path
            with open(link_file, 'w') as file:
                file.write(link_html % (url, cache_url, now))
            # replace real link to own file link
            bib['url'] = base_url + name

with open('auto.bib', 'w') as bibtex_file:
    bibtexparser.dump(bibtex_database, bibtex_file)
